<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>献立追加画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/common.css" rel="stylesheet">

</head>

<body>
<link href="css/menuAdd.css" rel="stylesheet">
  <!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row justify-content-end">

      <ul class="navbar-nav flex-row">
        <!-- ログインユーザの名前を表示の部分を${user.name } に変更-->
        <li class="nav-item"><span class="navbar-text">${userInfo.name}さん</span>
        </li>
      </ul>
    </nav>
  </header>
  <!-- /header -->

  <!-- body -->
  <div class="container-fluid">
    <div class="row mb-3">
      <div class="col">
        <h1 class="text-center">献立追加画面</h1>
      </div>
    </div>
    <!-- if文でリダイレクトしたらアラートするようにする ？-->
    <c:if test="${errMsg!=null}">
      <div class="row">
        <div class="col-6 offset-3 mb-5">
          <div class="alert alert-danger" role="alert">${errMsg}</div>
        </div>
      </div>
    </c:if>
    <form action="MenuAddServler" method="post">
      <div class="row">

        <div class="col-6 offset-3">

          <div class="form-group row">
            <label for="name" class="control-label col-3">料理名</label>
            <div class="col-9">
              <input type="text" name="name" id="menu-name" class="form-control" value="${name}">
            </div>
          </div>
          <div class="form-group row">
            <label for="password" class="control-label col-3">詳細</label>
            <div class="col-9">
              <input type="text" name="ingredients" id="ingredients" class="form-control" value="${ingredients}">
            </div>
          </div>
          <div class="form-group row">
            <label for="password-confirm" class="control-label col-3">URL</label>
            <div class="col-9">
              <input type="text" name="url" id="url" class="form-control" value="${url}">
            </div>
          </div>
          <div class="form-group row">
            <label for="password-confirm" class="control-label col-3">ジャンル</label>
            <div class="col-3">
              <input type="radio" name="category" value="japanese" class="menu"  >和食
            </div>
            <div class="col-3">
              <input type="radio" name="category" value="western" class="menu" class=" col-6 offset-6">洋食
            </div>
            <div class="col-3">
              <input type="radio" name="category" value="chinese" class="menu" class=" col-6 offset-3">中華
            </div>
          </div>
          <div>
            <button type="submit" value="登録" class="btn btn-primary btn-block form-submit">登録</button>
          </div>

          <div class="row mt-3">
            <div class="col">

              <a href="MenuListServlet">戻る</a>
            </div>
          </div>

        </div>

      </div>
    </form>
  </div>
</body>
</html>