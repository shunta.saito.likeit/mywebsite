<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ログイン画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/index.css" rel="stylesheet">
</head>

<body>
  <header>
    <div class="navbar navbar-dark"></div>
  </header>
  <!-- フォームが真ん中にレイアウトされるように、CSSに記載してある -->
  <div class="container">
    <div class="row">
      <div class="col-6 offset-3 mb-5">
        <h1 class="text-center">ログイン画面</h1>
      </div>
    </div>
    <c:if test="${errMsg!=null}">
      <div class="row">
        <div class="col-6 offset-3 mb-5">
          <div class="alert alert-danger" role="alert">${errMsg}</div>
        </div>
      </div>
    </c:if>
    <form action="LoginServlet" method="post">
      <div class="row">
        <div class="col-6 offset-3">
          <div class="form-group row">
            <label for="inputLoginId" class="col-3 col-form-label">ログインID</label>
            <div class="col-9">
              <input type="text" name="loginid" id="inputLoginId" class="form-control" value="${loginId}" autofocus>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword" class="col-3 col-form-label">パスワード</label>
            <div class="col-9">
              <input type="password" name="password" id="inputPassword" class="form-control" autofocus>
            </div>
          </div>

          <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
        </div>
      </div>
    </form>
    <div class="row mt-3">
      <div class="col-12 offset-8">
        <a href="UserAddServlet">新規登録</a>
      </div>
    </div>
  </div>
</body>

</html>