<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>献立検索画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/common.css" rel="stylesheet">
</head>

<body>
<link href="css/menuList.css" rel="stylesheet">
  <!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row justify-content-end">

      <ul class="navbar-nav flex-row">
        <li class="nav-item"><span class="navbar-text "> ${userInfo.name}さん</span>
        </li>
      </ul>
    </nav>
  </header>
  <!-- /header -->

  <div class="container">
    <div class="row">
      <div class="col">
        <h1 class="text-center">献立検索</h1>
      </div>
    </div>
    <!-- 検索ボックス -->
    <div class="row">
      <div class="col">
        <c:if test="${errMsg!=null}">
          <div class="row">
            <div class="col-6 offset-3 mb-5">
              <div class="alert alert-danger" role="alert">${errMsg}</div>
            </div>
          </div>
        </c:if>
        <div class="card">
          <div class="card-body">
            <form action="MenuListServlet" method="post">
              <div class="form-group row">
                <label for="loginId" class="col-2 col-form-label">料理名</label>
                <div class="col-10">
                  <input type="text" name="name" class="form-control" id="menu-name" value="${name}">
                </div>
              </div>

              <div class="form-group row">
                <label for="loginId" class="col-2 col-form-label">ジャンル</label>
                  <div class="col-8">
                    <div class="btn-group-toggle" data-toggle="buttons">
                      <div class="cp_ipcheck">
                        <div class="box">
                          <input type="checkbox" name="japanese" value="japanese" id="japanese" ${japanese!=null?"checked":""}  />
                          <label for="a_ch1">和食</label>
                          <input type="checkbox" name="western" value="western" id="western" ${western!=null?"checked":""} />
                          <!-- A = ”a”＝＝null？”B：C -->
                          
                          <label for="a_ch2">洋食</label>
                          <input type="checkbox" name="chinese" value="chinese" id="chinese" ${chinese!=null?"checked":""}/>
                          <label for="a_ch3">中華</label>
                          <input type="checkbox" name="random" value="random" id="random" ${random!=null?"checked":""}/>
                          <label for="a_ch4">ランダム</label>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>

              <div class="text-right">
                <button type="submit" class="btn btn-primary form-submit">検索</button>
              </div>
            </form>
          <c:if test="${userInfo.isAdmin == 1}">
            <div class="mt-3 text-right">
              <a class="btn btn-success" href="MenuAddServler">追加</a>
            </div>
            </c:if> 
          </div>
        </div>
      </div>
    </div>
    <!-- 検索結果一覧 -->
    <div class="row mt-3">
      <div class="col">
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr class="thead-dark">
              <th>ジャンル</th>
              <th>料理名</th>
              <th>主な食材</th>
              <th> </th>
            </tr>
          </thead>
          <tbody>

            <!-- For Each文のVarの値と下のtdで使うの~の値が一致していないと使えない。item=の括弧は()ではなく{}にする。 -->
            <c:forEach var="menu" items="${menuList}">
              <tr>
                <td>
                <c:choose> 
                <c:when test="menu.category == 1">和食</c:when>
                <c:when test="menu.category == 2">洋食</c:when>
                <c:when test="menu.category == 3">中華</c:when>
                </c:choose> 
                </td>
                <td><a href="${menu.url}"></a>${menu.name}</td>
                <td>${menu.ingredients}</td>
                <td class="float-right">
                 <c:if test="${userInfo.isAdmin == 1}">
                    <a class="btn btn-danger" href="MenuDeleteServlett?id=${menu.id}">削除</a>
                  </c:if> 
                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
    <ul class="navbar-nav flex-row">
      <li class="nav-item col-2 offset-2 mt-2">ユーザーデータ:</li>
      <li class="nav-item col-2"><a class="nav-link " href="UserUpdateServlet?id=${userInfo.id}">更新</a></li>
      <li class="nav-item col-2"><a class="nav-link " href="UserDeleteServlet?id=${userInfo.id}">削除</a></li>
      <li class="nav-item col-2"><a class="nav-link " href="LogoutServlet?id=${userInfo.id}">ログアウト</a></li>
    </ul>
  </div>
  
</body>

</html>