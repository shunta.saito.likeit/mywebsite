package model;

import java.io.Serializable;
import java.util.Date;


public class User implements Serializable {
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private int isAdmin;
  private int gender;

  public User() {}

  // ユーザー更新、削除機能を保存するためのコンストラクタ
  public User(int id, String name) {
    this.id = id;
    this.name = name;
  }

  // ログインセッションを保存するためのコンストラクタ
  public User(int id, String loginId, String name, int isAdmin) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.isAdmin = isAdmin;
  }

  // 全てのデータをセットするコンストラクタ
  public User(int id, String loginId, String name, Date birthDate, String password, int isAdmin,
      int gender) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
    this.gender = gender;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getIsAdmin() {
    return isAdmin;
  }

  public void setAdmin(int isAdmin) {
    this.isAdmin = isAdmin;
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

  public int getIsGender() {
    return gender;
  }
}
