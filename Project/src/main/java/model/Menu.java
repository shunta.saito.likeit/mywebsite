package model;

import java.io.Serializable;

public class Menu implements Serializable {
    private int id;
    private String name;
    private int category; 
    private String ingredients;
    private String url;
   
    public Menu() {}

    // メニュー削除機能に使うコンストラクタ
    public Menu(int id, String name) {
      this.id = id;
      this.name = name;
    }
    // 全てのデータをセットするコンストラクタ
    public Menu(int id, String name,int category,String ingredients,String url
      ) {
      this.id = id;
      this.name = name;
      this.category = category;
      this.ingredients = ingredients;
      this.url = url;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public int getCategory() {
      return category;
    }

    public void setCategory(int category) {
      this.category = category;
    }

    public String getIngredients() {
      return ingredients;
    }

    public void setIngredients(String ingredients) {
      this.ingredients = ingredients;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
}
