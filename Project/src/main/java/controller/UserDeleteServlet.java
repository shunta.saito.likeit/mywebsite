package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      // sessionのデータはUserだからUserにキャスト
      User session_user = (User) session.getAttribute("userInfo");
      if (session_user == null) {
        response.sendRedirect("LoginServlet");
      } else {
        String id = request.getParameter("id");
        // 確認用：idをコンソールに出力
        System.out.println(id);
        UserDao userDao = new UserDao();
        User userDteil = userDao.userDetail(id);
        // ユーザidを元にどのユーザーがクリックしたか確認する。
        request.setAttribute("userDteil", userDteil);
        // ユーザ削除jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
        dispatcher.forward(request, response);
      }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      String id = request.getParameter("Id");
      // 確認用：idをコンソールに出力
      System.out.println(id);
      UserDao userDao = new UserDao();

      userDao.userDelete(id);
      HttpSession session = request.getSession();
      session.setAttribute("userDelete", userDao);

      // 献立検索のサーブレットにリダイレクト
      response.sendRedirect("LogoutServlet");
	}
}
