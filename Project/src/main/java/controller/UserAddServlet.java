package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import controller.util.PasswordEncorder;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインしていなかったらログイン画面に遷移。
      HttpSession session = request.getSession();
      // sessionのデータはUserだからUserにキャストする。
      User session_user = (User) session.getAttribute("userInfo");
      if (session_user == null) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
       } else {
         // 献立検索のサーブレットにリダイレクト? 遷移にする？
        response.sendRedirect("MenuListServlet");
      }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      // リクエストパラメータの入力項目を取得。
      String loginId = request.getParameter("loginId");
      String password = request.getParameter("password");
      String password_confirm = request.getParameter("password-confirm");
      String name = request.getParameter("name");
      String birth_date = request.getParameter("birth_date");
      String gender = request.getParameter("gender");// String male = request.getParameter("male")
      // String female = request.getParameter("female");
      // String non_response = request.getParameter("non_response");

      UserDao userDao = new UserDao();
      // 例外開始。入力したログインIDが既にDBに既にあったら
      User sloginId = userDao.findByLoginInfo(loginId, password);
      if (sloginId != null) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("gender", gender);

        // request.setAttribute("male", male);
        // request.setAttribute("female", female);
        // request.setAttribute("non_response", non_response);

        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // パスワードとパスワード(確認)の値が一致していなかったら
      if (!password.equals(password_confirm)) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "パスワードが一致していません");

        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("gender", gender);
        // request.setAttribute("male", male);
        // request.setAttribute("female", female);
        // request.setAttribute("non_response", non_response);


        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      // 一つでも未入力があったらエラー
      if (loginId.equals("") || password.equals("") || password_confirm.equals("")
          || name.equals("") || birth_date.equals("")
          || gender == null) {
        // && female.equals("") && non_response.equals("")
        // リクエストスコープにエラーメッセージをセット

        request.setAttribute("errMsg", "入力された文字は正しくありません");
        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("gender", gender);
        // request.setAttribute("male", male);
        // request.setAttribute("female", female);
        // request.setAttribute("non_response", non_response);

        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // 暗号化
      String encodestr = PasswordEncorder.main(password);

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      userDao.userAdd(loginId, encodestr, name, birth_date, gender);
      /** テーブルに該当のデータが登録できた場合 * */

      // ユーザ一覧のサーブレットにリダイレクト 一回の Menuに行ってないから遷移？
      response.sendRedirect("MenuListServlet");
    }
}
