package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.MenuDao;
import model.Menu;
import model.User;

/**
 * Servlet implementation class MenuAddServler
 */
@WebServlet("/MenuAddServler")
public class MenuAddServler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuAddServler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  HttpSession session = request.getSession();
	  User session_user = (User) session.getAttribute("userInfo");
      // ログインIDがadminだったらメニューの追加画面に遷移する。
      if (session_user.getLoginId() != "admin") {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/menuAdd.jsp");
        dispatcher.forward(request, response);
       } else {
         // 献立検索のサーブレットにリダイレクト? 遷移にする？
        response.sendRedirect("MenuListServlet");
      }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");
      // リクエストパラメータの入力項目を取得。
      String name = request.getParameter("name");
      String ingredients = request.getParameter("ingredients");
      String url = request.getParameter("url");
      String category = request.getParameter("category");

      MenuDao menuDao = new MenuDao();
      // 例外開始。入力した献立が既にDBに既にあったら(メソッドはまで作ってない)
      //別のページで別の名前で同じ名前の料理を表示したいならnameの値は引数にしない。
      //そうするなら名前を表示するんじゃなくてキャッチフレーズ？を表示にする？

      Menu menuAlready = menuDao.findByMenuAlready(name, ingredients, url, category);
      if (menuAlready != null) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("name", name);
        request.setAttribute("ingredients", ingredients);
        request.setAttribute("url", url);
        request.setAttribute("category", category);

        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      
      if (name.equals("") || ingredients.equals("") || url.equals("") || category.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");
        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("name", name);
        request.setAttribute("ingredients", ingredients);
        request.setAttribute("url", url);
        request.setAttribute("category", category);
        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        menuDao.menuAdd(name, ingredients, url, category);
        
        /** テーブルに該当のデータが登録できた場合 * */
        // ユーザ一覧のサーブレットにリダイレクト 一回の Menuに行ってないから遷移？
        response.sendRedirect("MenuListServlet");
	}
}
