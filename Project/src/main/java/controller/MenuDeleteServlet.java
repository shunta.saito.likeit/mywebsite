package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.MenuDao;
import model.Menu;
import model.User;

/**
 * Servlet implementation class MenuDeleteServlet
 */
@WebServlet("/MenuDeleteServlet")
public class MenuDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      // sessionのデータはUserだからUserにキャスト
      User session_user = (User) session.getAttribute("userInfo");
      // ログインしているユーザーがadminだったらDeletel.jspに遷移。
      if (session_user.getLoginId() == "admin") {
        String id = request.getParameter("id");
        // 確認用：idをコンソールに出力
        System.out.println(id);
        MenuDao menuDao = new MenuDao();
        Menu menuDteil = menuDao.menuDetail(id);
        // ユーザidを元にどのユーザーがクリックしたか確認する。
        request.setAttribute("menuDteil", menuDteil);
        // ユーザ削除jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/menuDelete.jsp");
        dispatcher.forward(request, response);
      } else {
        // 違ったらメニューサーブレットにリダイレクト。
        response.sendRedirect("MenuListServlet");
      }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      // 雛形はuserdelete
      request.setCharacterEncoding("UTF-8");

      String id = request.getParameter("Id");
      // 確認用：idをコンソールに出力
      System.out.println(id);
      MenuDao menuDao = new MenuDao();

      menuDao.menuDelete(id);
      HttpSession session = request.getSession();
      session.setAttribute("menuDelete", menuDao);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("MenuListServlet");
	}

}
