package controller.util;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;
public class PasswordEncorder {

  public static String main(String args) {
    Scanner scan = new Scanner(args);

    //変換したい文字列を入力
    String str = scan.next();

    //ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    //ハッシュ関数の種類(今回はMD5)
    String algorithm = "MD5";
    
    //ハッシュ生成処理
    try {
        byte[] bytes = MessageDigest.getInstance(algorithm).digest(str.getBytes(charset));

        final String encodestr = DatatypeConverter.printHexBinary(bytes);
        // 暗号化結果を返す。
        System.out.println(encodestr);
        return encodestr;
    } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
        return null;
    }
  }
}
