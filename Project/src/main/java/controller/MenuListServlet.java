package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.MenuDao;
import model.Menu;
import model.User;

/** Servlet implementation class UserListServlet */
@WebServlet("/MenuListServlet")
public class MenuListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public MenuListServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    // sessionのデータはUserだからUserにキャスト
    User session_user = (User) session.getAttribute("userInfo");
    // ログインセッションがない場合、ログイン画面にリダイレクトさせる
    if (session_user == null) {
      response.sendRedirect("LoginServlet");
    } else {
       // メニュー検索画面のjspにフォワード
       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/menuList.jsp");
       dispatcher.forward(request, response);
     }
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO 未実装：検索処理全般
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");
    // リクエストパラメータを取得
    String name = request.getParameter("name");
    String japanese = request.getParameter("japanese");
    String western = request.getParameter("western");
    String chinese = request.getParameter("chinese");
    String random = request.getParameter("random");

    MenuDao menuDao = new MenuDao();
    List<Menu> menuList = new ArrayList<Menu>();
    Menu menu = new Menu();
    
    //検索条件にあうメニューのリストを取得
    //カテゴリーから検索したい時
    // if (name.equals("") || japanese != null || western != null
    // || chinese != null && random == null) {
    if(random != null) {
      menuList = menuDao.findByMenuListRandom(random);
      // 実行した後自分が何を検索したかわかるように入力した値をセット
      Collections.shuffle(menuList);
      List<Menu> menuListRandom = new ArrayList<Menu>();
      // 新しいリストに入れるのを3回繰り返す。
      for (int i = 0; i < 3; i++) {
        menu = (Menu) menuList.get(i);
        menuListRandom.add(menu);
      }
      request.setAttribute("menuList", menuListRandom);
    }else if(name.equals("") && japanese == null && western == null && chinese == null) {
      System.out.println("何も入力されてない場合");
      menuList = null;
      request.setAttribute("menuList", menuList);
      
    } else {
      System.out.println("ランダムなし");
      menuList = menuDao.findByMenuListByCategory(japanese, western, chinese);
      request.setAttribute("menuList", menuList);
    }
    
    // for(Menu recipe :menuList) {
    // System.out.println("recipe: " + recipe.getName());
    // }

    System.out.println("* * * * * * * * * * * * * * * ");

      // 実行した後自分が何を検索したかわかるように入力した値をセット
      request.setAttribute("name", name);
      request.setAttribute("japanese", japanese);
      request.setAttribute("western", western);
      request.setAttribute("chinese", chinese);
      request.setAttribute("random", random);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/menuList.jsp");
      dispatcher.forward(request, response);
  }
}