package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Menu;

public class MenuDao {

  // メソッドの名前は変更雛形はuserAdd 検索機能 雛形はuser検索機能
  /*
   * public List<User> findByUser(String loginId, String name, String date_start, String date_end) {
   * Connection conn = null; List<User> userList = new ArrayList<User>(); try { // データベースへ接続
   * 
   * conn = DBManager.getConnection();
   * 
   * // SELECT * FROM user WHERE を基準にしてそれに連結させる? ArrayList<String> paramList = new ArrayList<>();
   * 
   * StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE is_admin = false");
   * 
   * if (!loginId.equals("")) { sql.append(" and login_id = ? "); paramList.add(loginId); }
   * 
   * if (!name.equals("")) { sql.append(" and name LIKE ? "); paramList.add(name); }
   * 
   * if (!date_start.equals("")) { sql.append(" and birth_date >= ? "); paramList.add(date_start); }
   * 
   * if (!date_end.equals("")) { sql.append(" and birth_date <= ? "); paramList.add(date_end); }
   * 
   * // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う String sql1 = sql.toString(); PreparedStatement pStmt =
   * conn.prepareStatement(sql1); for (int i = 1; i - 1 < paramList.size(); i++) {
   * pStmt.setString(i, "%" + paramList.get(i - 1) + "%"); } ResultSet rs = pStmt.executeQuery();
   * 
   * // 必要なデータのみインスタンスのフィールドに追加 while (rs.next()) { int id = rs.getInt("id"); String loginid =
   * rs.getString("login_id"); String Name = rs.getString("name"); Date birthDate =
   * rs.getDate("birth_date"); String password = rs.getString("password"); boolean isAdmin =
   * rs.getBoolean("is_admin"); Timestamp createDate = rs.getTimestamp("create_date"); Timestamp
   * updateDate = rs.getTimestamp("update_date"); User user = new User(id, loginid, Name, birthDate,
   * password, isAdmin, createDate, updateDate);
   * 
   * userList.add(user); } } catch (SQLException e) { e.printStackTrace(); return null; } finally {
   * // データベース切断 if (conn != null) { try { conn.close(); } catch (SQLException e) {
   * e.printStackTrace(); return null; } } } return userList; }
   */

  // 全てのメニューからランダム検索
  public List<Menu> findByMenuListRandom(String ramdam) {

    Connection conn = null;
    List<Menu> menuListRandom = new ArrayList<Menu>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM menu ";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String ingredients = rs.getString("ingredients");
        String url = rs.getString("url");
        int category = rs.getInt("category");
        System.out.println(url);

        Menu menu = new Menu(id, name, category, ingredients, url);

        menuListRandom.add(menu);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return menuListRandom;
  }

  // ジャンルの中から選択
  public List<Menu> findByMenuListByCategory(String japanese, String western, String chinese) {

    Connection conn = null;
    List<Menu> menuListCategory = new ArrayList<Menu>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // ArrayList<String> paramList = new ArrayList<>();
      StringBuilder sql = new StringBuilder("SELECT * FROM menu ");

      System.out.println("japanese: " + japanese);

      boolean alreadyChosen = false;
      if (japanese != null) {
        // paramList.add(japanese);
        sql.append(" WHERE category = 1");
        alreadyChosen = true;
      }

      if (western != null) {
        // paramList.add(japanese);
        if (!alreadyChosen) {
          sql.append(" WHERE ");
        }
        sql.append(" category = 2");
        alreadyChosen = true;
      }

      if (chinese != null) {
        // paramList.add(japanese);
        if (!alreadyChosen) {
          sql.append(" WHERE ");
        }
        sql.append(" category = 3");
        alreadyChosen = true;

      }

        // 和食が選択されずに洋食が選択された場合。

        String sql_Str = sql.toString();
        PreparedStatement pStmt = conn.prepareStatement(sql_Str);
        // for (int i = 1; i - 1 < paramList.size(); i++) {
        // pStmt.setString(i, paramList.get(i - 1));
        // }
      // SELECTを実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();
      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int category = rs.getInt("category");
        String ingredients = rs.getString("ingredients");
        String url = rs.getString("url");
        Menu menu = new Menu(id, name, category, ingredients, url);

        menuListCategory.add(menu);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return menuListCategory;
  }


  // 既にDBに献立が登録されているか調べる。
  public Menu findByMenuAlready(String name, String category, String ingredients, String url) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT * FROM menu WHERE name = ? and ingredients = ? and url = ? and category = ? ";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, ingredients);
      pStmt.setString(3, url);
      pStmt.setString(4, category);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id");
      String Name = rs.getString("name");
      return new Menu(id, Name);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // メニューの追加
  public void menuAdd(String name, String ingredients, String url, String category) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      int categoryValue = 0;
      String sql = "INSERT INTO menu (name, ingredients, url,category) VALUES(?,?,?,?)";
      // categoryの値がJapanなら1 Westernなら2 Chineseなら3
      if (category.equals("japanese")) {
        categoryValue = 1;
      } else if (category.equals("western")) {
        categoryValue = 2;
      } else if (category.equals("chinese")) {
        categoryValue = 3;
      }

      // SELECTを実行し、結果表を取得
      String sql_date = sql.toString();
      PreparedStatement pStmt = conn.prepareStatement(sql_date);
      pStmt.setString(1, name);
      pStmt.setString(2, ingredients);
      pStmt.setString(3, url);
      pStmt.setInt(4, categoryValue);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // メニューの削除機能
  public void menuDelete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "DELETE FROM menu WHERE id = ? ";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);

      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public Menu menuDetail(String Id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM menu WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, Id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id");
      String name = rs.getString("name");
      return new Menu(id, name);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}
