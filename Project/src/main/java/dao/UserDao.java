package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id");
      String login_id = rs.getString("login_id");
      String nameData = rs.getString("name");
      int isAdmin = rs.getInt("is_admin");
      return new User(id, login_id, nameData, isAdmin);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // findByのコピペ
  public void userAdd(String loginId, String password, String name, String birth_date,
      String gender) {
    System.out.println("UserDAO, userAdd内");

    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user (login_id, password, name, birth_date, gender) VALUES (?,?,?,?,?)";

      System.out.println("gender: " + gender);
      int genderValue = 0;
      // categoryの値がmaleなら1 femaleなら2 non-responseなら3
      if (gender.equals("male")) {
        genderValue = 1;
      } else if (gender.equals("female")) {
        genderValue = 2;
      } else if (gender.equals("non_response")) {
        genderValue = 3;
      }
      System.out.println("genderValue: " + genderValue);

      System.out.println("preparedStatementの前まで来ました！");
      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      pStmt.setString(3, name);
      pStmt.setString(4, birth_date);
      pStmt.setInt(5, genderValue);

      int result = pStmt.executeUpdate();
      System.out.println("result: " + result);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 部分アップデート雛形はallUpdateメソッド
  public void Update(String password, String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "UPDATE user SET password = ? WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, password);
      pStmt.setString(2, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // メソッドの名前、引数、SQL文の実行部分の変更、雛形はfindby これってvoid?
  public void userDelete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "DELETE FROM user WHERE id = ? ";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      
      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // ユーザー更新、削除機能に必要なユーザーデータを取得するメソッド。雛形はlogin
  public User userDetail(String ID) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, ID);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      int id = rs.getInt("id");
      String name = rs.getString("name");
      return new User(id, name);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}


